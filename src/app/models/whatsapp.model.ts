export class WhatsApp {
    constructor(
        public sid: string,
        public to: string,
        public body: string,
        public status: string
    ) { }
}