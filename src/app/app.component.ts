import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import {TestFormService} from './services/test-form.service'

import { WhatsApp } from './models/whatsapp.model'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TestFormService]
})
export class AppComponent {
  title = 'app';
  whatsAppModel: WhatsApp
  formWhatsapp: FormGroup

  constructor(private _testService: TestFormService) {
    this._testService.sendWhatsApp('+5216861585036', 'jeje')
      .subscribe(data => this.whatsAppModel = data,
      err => console.log(err))

      this.formWhatsapp = new FormGroup({
        to: new FormControl(''),
        message: new FormControl('')
      })
  }

  sendWhatsapp() {
    alert('enviado')
  }
}
