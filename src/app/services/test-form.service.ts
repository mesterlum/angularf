import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { WhatsApp } from '../models/whatsapp.model'
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class TestFormService {

  constructor(private _http: HttpClient) { }

  sendWhatsApp(to: string, messsage: string):  Observable<WhatsApp> {
    return this._http.post<WhatsApp>(`${environment.host}/whatsapp.php`, {to,messsage})
      .pipe(
        map((response: WhatsApp) => response)
      )
  }
}
